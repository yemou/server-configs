{ pkgs, ... }:
let
  cpCerts = pkgs.writeShellApplication {
    name = "cpCerts";
    runtimeInputs = with pkgs; [ coreutils ];
    text = ''
      prog_name=''${0##*/}

      case $1 in
        "proxy.butwho.org" | "conference.butwho.org" | "pubsub.butwho.org" | \
        "upload.butwho.org" | "butwho.org" )
          caddy_path="/var/lib/caddy/.local/share/caddy"
          printf '%s\n' "$prog_name: Copying certs for '$1' to /sss/certs/$1"

          mkdir -p "/sss/certs/$1"
          cp -f "$caddy_path/$2" "/sss/certs/$1"
          cp -f "$caddy_path/$3" "/sss/certs/$1"

          # Read both the .crt file and the .key file to make a .pem
          crt=$(cat "$caddy_path/$2")
          key=$(cat "$caddy_path/$3")
          printf '%s\n\n%s\n' "$crt" "$key" > "/sss/certs/$1/$1.pem"

          printf '%s\n' "$prog_name: Copied certs for '$1'"
        ;;
        * ) printf '%s\n' "$prog_name: Skipping $1" ;;
      esac
    '';
  };
  cpCertsCommand = "${cpCerts}/bin/cpCerts {event.data.identifier} {event.data.certificate_path} "
    + "{event.data.private_key_path}";
in
{
  environment.persistence."/data/persistent".directories = [{
    directory = "/var/www/org.butwho";
    user = "mou";
    group = "caddy";
    mode = "0750";
  }];

  services.caddy = {
    package = pkgs.callPackage ../../modules/packages/caddy-plugins.nix { };
    globalConfig = ''
      events {
        on cert_obtained exec ${cpCertsCommand}
      }
    '';
    virtualHosts = {
      "butwho.org".extraConfig = ''
        encode zstd gzip

        # Should I set this for all .well-known files?
        # header /.well-known/* Access-Control-Allow-Origin *

        # Unsure if this works without
        # header /.well-known/host-meta* Access-Control-Allow-Origin *

        header /.well-known/host-meta Content-Type text/xml
        respond /.well-known/host-meta <<XML
          <?xml version='1.0' encoding='utf-8'?>
          <XRD xmlns='http://docs.oasis-open.org/ns/xri/xrd-1.0'>
            <Link rel="urn:xmpp:alt-connections:xbosh" href="https://butwho.org:5443/bosh" />
            <Link rel="urn:xmpp:alt-connections:websocket" href="wss://butwho.org:5443/ws" />
          </XRD>
          XML

        header /.well-known/host-meta.json Content-Type application/json
        respond /.well-known/host-meta.json <<JSON
          {
            "links": [
              { "rel": "urn:xmpp:alt-connections:xbosh", "href": "https://butwho.org:5443/bosh" },
              { "rel": "urn:xmpp:alt-connections:websocket", "href": "wss://butwho.org:5443/ws" }
            ]
          }
          JSON

        header /.well-known/matrix/* Access-Control-Allow-Origin *
        header /.well-known/matrix/* Content-Type application/json
        respond /.well-known/matrix/client <<JSON
          {
            "m.homeserver": { "base_url": "https://matrix.butwho.org" }
          }
          JSON
        respond /.well-known/matrix/server `{"m.server": "matrix.butwho.org:443"}`

        root * /var/www/org.butwho
        file_server {
          hide .git readme.md license
        }

        handle_errors {
          respond "{err.status_code} {err.status_text}"
        }
      '';
      "matrix.butwho.org".extraConfig = ''
        encode zstd gzip
        reverse_proxy 100.77.30.206:8008
      '';
      "conference.butwho.org".extraConfig = ''
        redir https://butwho.org{uri}
      '';
      "proxy.butwho.org".extraConfig = ''
        redir https://butwho.org{uri}
      '';
      "pubsub.butwho.org".extraConfig = ''
        redir https://butwho.org{uri}
      '';
      "upload.butwho.org".extraConfig = ''
        redir https://butwho.org{uri}
      '';
      "cloud.lilac.pink".extraConfig = ''
        encode zstd gzip
        reverse_proxy 100.77.30.206:80 {
          # NixOS uses nginx as a webserver for NextCloud by default and nginx will send 301 redirects to the client
          # using http as the protocol instead of https since it doesn't have SSL certs of its own.
          # Because the redirect isn't also https, this can cause a Content-Security-Policy error on the client.
          # In practice this prevents us from enabling apps on NextCloud without this line.
          header_down Location http:// https://
        }
      '';
    };
  };
}
