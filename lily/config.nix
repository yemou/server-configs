{ ... }: {
  imports = [
    ./hardware.nix
    ./packages.nix

    ../modules/basic.nix
    ../modules/development
    ../modules/editor.nix
    ../modules/machine-id.nix
    ../modules/machineInfo.nix
    ../modules/netbird.nix
    ../modules/nix.nix
    ../modules/remoteBuilder.nix

    ../modules/services/dendrite.nix
    ../modules/services/esquid.nix
    ../modules/services/libvirt.nix
    ../modules/services/nextcloud.nix
    ../modules/services/openssh.nix
  ];

  mInfo = let hidden = builtins.fromJSON (builtins.readFile ./hidden.json); in {
    ipv4 = "192.168.1.92";
    ipv6 = hidden.ipv6;
    nb-ipv4 = "100.77.30.206";
  };

  sops = {
    defaultSopsFile = ../secrets/lily.yaml;
    defaultSopsFormat = "yaml";
    age.keyFile = "/data/keys.txt";
  };

  networking.hostName = "lily";

  services = {
    smartd.enable = true;
    thermald.enable = true;
  };

  nix.settings = {
    cores = 4;
    max-jobs = 2;
  };

  # This option defines the first version of NixOS you have installed on this particular machine,
  # and is used to maintain compatibility with application data (e.g. databases) created on older
  # NixOS versions. Most users should NEVER change this value after the initial install, for any
  # reason, even if you've upgraded your system to a new NixOS release. This value does NOT affect
  # the Nixpkgs version your packages and OS are pulled from, so changing it will NOT upgrade your
  # system. This value being lower than the current NixOS release does NOT mean your system is out
  # of date, out of support, or vulnerable. Do NOT change this value unless you have manually
  # inspected all the changes it would make to your configuration, and migrated your data
  # accordingly. For more information, see `man configuration.nix` or
  # https://nixos.org/manual/nixos/stable/options#opt-system.stateVersion .
  system.stateVersion = "24.05"; # Did you read the comment?
}
