{ config, ... }: {
  imports = [ ./postgresql.nix ];

  nixpkgs.overlays = [
    (final: prev: {
      dendrite = prev.dendrite.overrideAttrs {
        version = "0.13.8-df770dae0aa823e2dcba7c6d8682da60c679dfde";

        src = prev.fetchFromGitHub {
          owner = "matrix-org";
          repo = "dendrite";
          rev = "df770dae0aa823e2dcba7c6d8682da60c679dfde";
          hash = "sha256-VA+fwIxlTVrQpQE0OimwS3DRJfQCCQC4uLJgj7sb8QA=";
        };

        vendorHash = "sha256-rGOB1ikY3BgChvD1YZUF66g8P6gE29b/k9kxvHR0+WQ=";
      };
    })
  ];

  sops = {
    secrets."dendrite/registration_shared_secret" = { };
    templates.dendrite-env.content = ''
      REGISTRATION_SHARED_SECRET=${config.sops.placeholder."dendrite/registration_shared_secret"}
    '';
  };

  environment.persistence."/data/persistent".directories = [{
    directory = "/var/lib/private/dendrite";
    mode = "0700";
  }];

  networking.firewall.interfaces.wt0.allowedTCPPorts = [ 8008 ];

  services.postgresql = {
    ensureDatabases = [ "dendrite" ];
    ensureUsers = [{
      name = "dendrite";
      ensureDBOwnership = true;
    }];
  };

  systemd.services.dendrite.requires = [ "postgresql.service" ];

  services.dendrite = {
    enable = true;
    environmentFile = config.sops.templates.dendrite-env.path;
    settings = {
      global = {
        private_key = "$CREDENTIALS_DIRECTORY/private_key";
        server_name = "butwho.org";
        database.connection_string = "postgresql:///dendrite?host=/run/postgresql";
        presence = {
          enable_inbound = true;
          enable_outbound = true;
        };
        dns_cache.enable = true;
      };
      app_service_api.database.connection_string = "";
      client_api = {
        registration_disabled = true;
        guests_disabled = true;
        registration_shared_secret = "$REGISTRATION_SHARED_SECRET";
        enable_registration_captcha = false; # I need keys for this. Use hcaptcha?
      };
      federation_api.database.connection_string = "";
      media_api = {
        max_file_size_bytes = 104857600;
        dynamic_thumbnails = true;
        database.connection_string = "";
      };
      mscs = {
        mscs = [ "msc2836" ];
        database.connection_string = "";
      };
      sync_api = {
        real_ip_header = "X-Forwarded-For";
        search.enabled = true;
        database.connection_string = "";
      };
      user_api = {
        device_database.connection_string = "";
        account_database.connection_string = "";
      };
      room_server.database.connection_string = "";
      relay_api.database.connection_string = "";
      key_server.database.connection_string = "";
      logging = [
        { type = "std"; level = "warn"; }
        { type = "file"; level = "info"; params.path = "./logs"; }
      ];
    };
    openRegistration = false;
    loadCredential = [ "private_key:/var/lib/private/dendrite/matrix_key.pem" ];
  };
}
