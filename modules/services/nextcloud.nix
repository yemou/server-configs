{ config, lib, pkgs, ... }: {
  environment.persistence."/data/persistent".directories = [
    {
      directory = "/var/lib/nextcloud";
      mode = "0700";
      user = "nextcloud";
      group = "nextcloud";
    }
    {
      directory = "/var/lib/redis-nextcloud";
      mode = "0700";
      user = "nextcloud";
      group = "nextcloud";
    }
  ];

  sops = {
    secrets = {
      "nextcloud/adminPass" = {
        owner = "nextcloud";
        group = "nextcloud";
      };
      "smtp/user" = { };
      "smtp/pass" = { };
    };
    templates.smtpConfig = {
      owner = "nextcloud";
      group = "nextcloud";
      content = builtins.toJSON {
        mail_domain = "lilac.pink";
        mail_from_address = "nextcloud-noreply";
        mail_smtpauth = true;
        mail_smtphost = "smtp.purelymail.com";
        mail_smtpname = config.sops.placeholder."smtp/user";
        mail_smtppassword = config.sops.placeholder."smtp/pass";
        mail_smtpport = 465;
        mail_smtpsecure = "ssl";
      };
    };
  };

  # This is the port that nginx listens on by default
  networking.firewall.interfaces.wt0.allowedTCPPorts = [ 80 ];

  services = {
    redis.package = pkgs.valkey;
    nextcloud = {
      enable = true;
      package = pkgs.nextcloud30;
      appstoreEnable = true;
      autoUpdateApps.enable = true;
      caching = {
        apcu = true;
        redis = true;
      };
      config = {
        adminpassFile = config.sops.secrets."nextcloud/adminPass".path;
        adminuser = "admin";
        dbtype = "pgsql";
      };
      configureRedis = true;
      database.createLocally = true;
      enableImagemagick = true;
      # extraApps = { };
      extraAppsEnable = false;
      hostName = "cloud.lilac.pink";
      https = true;
      maxUploadSize = "1G";
      notify_push.enable = true;
      phpOptions."opcache.interned_strings_buffer" = "16";
      # poolConfig = '' '';
      # poolSettings = { };
      secretFile = config.sops.templates.smtpConfig.path;
      settings = {
        dbpersistent = true;
        default_phone_region = "US";
        "gs.federation" = "global";
        "htaccess.RewriteBase" = "/";
        "maintenance_window_start" = 1;
        "memcache.locking" = ''\OC\Memcache\Redis'';
        "overwrite.cli.url" = "https://cloud.lilac.pink";
        overwritehost = "cloud.lilac.pink";
        overwriteprotocol = "https";
        overwritewebroot = "/";
        redis = {
          host = "/run/redis-nextcloud/redis.sock";
          port = 0;
          timeout = "1.5";
        };
        "simpleSignUpLink.shown" = false;
        trusted_proxies = [
          "100.77.19.103"
          config.mInfo.nb-ipv4
        ];
      };
    };
  };

  systemd.services.nextcloud-notify_push.environment.NEXTCLOUD_URL = lib.mkForce "http://${config.mInfo.nb-ipv4}";
}
