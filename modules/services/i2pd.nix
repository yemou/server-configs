{ ... }: {
  environment.persistence."/data/persistent".directories = [{
    directory = "/var/lib/i2pd";
    user = "i2pd";
    group = "i2pd";
    mode = "0700";
  }];

  networking.firewall = {
    allowedUDPPorts = [ 28381 ];
    # interfaces.wt0.allowedTCPPorts = [
    #   4444 # httpProxy
    #   # 4447 # socksProxy
    #   # 7659 # SAM
    #   # 7654 # I2CP
    # ];
  };

  services.i2pd = {
    enable = true;
    port = 28381;
    enableIPv6 = true;
    bandwidth = 2048;
    ntcp2.published = true;
    proto = {
      http.enable = true;
      # httpProxy = {
      #   enable = true;
      #   address = "0.0.0.0";
      # };
      # socksProxy = {
      #   enable = true;
      #   # address = "0.0.0.0";
      # };
      # sam = {
      #   enable = true;
      #   # address = "0.0.0.0";
      # };
      # i2cp = {
      #   enable = true;
      #   # address = "0.0.0.0";
      # };
    };
  };
}
