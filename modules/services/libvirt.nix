{ pkgs, ... }: {
  hardware.graphics.enable = true;
  users.users.mou.extraGroups = [ "libvirtd" "kvm" ];
  environment.persistence."/data/persistent".directories = [ "/var/lib/libvirt" ];

  virtualisation.libvirtd = {
    enable = true;
    qemu = {
      swtpm.enable = true;
      ovmf = {
        enable = true;
        packages = [
          (pkgs.OVMF.override {
            secureBoot = true;
            tpmSupport = true;
          }).fd
        ];
      };
    };
  };
}
