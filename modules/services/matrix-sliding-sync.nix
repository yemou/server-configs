{ config, ... }: {
  sops = {
    secrets."matrix-sliding-sync/secret" = { };
    templates.matrix-sliding-sync-env.content = ''
      SYNCV3_SECRET=${config.sops.placeholder."matrix-sliding-sync/secret"}
    '';
  };

  networking.firewall.interfaces.wt0.allowedTCPPorts = [ 8009 ];

  services.matrix-sliding-sync = {
    enable = true;
    environmentFile = config.sops.templates.matrix-sliding-sync-env.path;
    settings = {
      SYNCV3_BINDADDR = "[::]:8009";
      SYNCV3_SERVER = "https://matrix.butwho.org";
    };
  };
}
