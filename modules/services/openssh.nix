{ ... }: {
  environment.persistence."/data/persistent".files = [
    "/etc/ssh/ssh_host_ed25519_key"
    "/etc/ssh/ssh_host_ed25519_key.pub"
    "/etc/ssh/ssh_host_rsa_key"
    "/etc/ssh/ssh_host_rsa_key.pub"
  ];

  networking.firewall.interfaces.wt0.allowedTCPPorts = [ 22 ];

  services.openssh = {
    enable = true;
    ports = [ 22 ];
    openFirewall = false;
    settings = {
      PasswordAuthentication = false;
      PermitRootLogin = "no";
    };
  };

  users.users = {
    mou.openssh.authorizedKeys.keys = [
      "ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIKnyBRVRLKrlsAlMFXimvcF/mBjmSfixdzUX4yCZsYvE lutea"
    ];
    seedling.openssh.authorizedKeys.keys = [
      "ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAINBZ1NlSJbQPkMxk+jW2Gh2DWBDGZ5mRvROqwbCrdrui seedling@dandelion"
      "ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIDAyk3iOL/ax+EV1Ubo0SQy6kPwcjVCxwRQb9g0IKj6Z seedling@lily"
    ];
  };
}
