{ ... }: {
  environment.persistence."/data/persistent".directories = [
    {
      directory = "/var/lib/mysql";
      user = "mysql";
      group = "mysql";
      mode = "0700";
    }
    {
      directory = "/var/lib/seafile";
      user = "seafile";
      group = "seafile";
      mode = "0700";
    }
  ];

  networking.firewall.interfaces.wt0.allowedTCPPorts = [ 8000 8082 ];

  services.seafile = {
    enable = true;
    adminEmail = "seafile.admin@lilac.pink";
    initialAdminPassword = "IfYouUseThisPasswordAndItWorksIAmProbablyDumb";
    gc.enable = true;
    ccnetSettings.General.SERVICE_URL = "https://seafile.lilac.pink";
    seafileSettings.fileserver.host = "0.0.0.0";
    seahubAddress = "0.0.0.0:8000";
    seahubExtraConf = ''
      ENABLE_SETTINGS_VIA_WEB = False
    '';
  };
}
