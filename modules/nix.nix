{ pkgs, ... }: {
  systemd = {
    services.nix-daemon.environment.TMPDIR = "/nix/tmp";
    tmpfiles.rules = [ "d /nix/tmp - root root 1d" ];
  };

  users.users.mou.packages = [
    (pkgs.callPackage ./packages/nixos-rebuild-tmpdir.nix { })
    (pkgs.callPackage ./packages/buildConfig { })
  ];

  nix = {
    optimise.automatic = true;
    gc.automatic = true;
    settings = {
      auto-optimise-store = true;
      experimental-features = [ "nix-command" "flakes" ];
      use-xdg-base-directories = true;
    };
  };
}
