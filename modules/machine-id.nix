{ config, ... }: {
  # /etc/machine-id needs to be readable by everyone
  sops.secrets."machine-id".mode = "0444";
  environment.etc.machine-id.source = config.sops.secrets."machine-id".path;
}
