{ lib, ... }: {
  options.mInfo = {
    ipv4 = lib.mkOption {
      type = with lib.types; nullOr str;
      default = null;
      description = "IPv4 address of the local machine";
    };
    ipv6 = lib.mkOption {
      type = with lib.types; nullOr str;
      default = null;
      description = "IPv6 address of the local machine";
    };
    nb-ipv4 = lib.mkOption {
      type = with lib.types; nullOr str;
      default = null;
      description = "NetBird IPv4 address of the local machine";
    };
  };
}
