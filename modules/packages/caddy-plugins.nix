# comes from https://github.com/emilylange/nixos-config/blob/6f53d301ff77b0f0d24b9d8632bf0c4298d99aaf/packages/caddy/default.nix
# TODO: move to https://github.com/NixOS/nixpkgs/pull/317881 when merged
{ lib
, caddy
, xcaddy
, buildGoModule
, stdenv
, cacert
, go
}:
let
  version = "2.8.4";
  rev = "v${version}";
in
(caddy.overrideAttrs (_: { inherit version; })).override {
  buildGoModule = args: buildGoModule (args // {
    src = stdenv.mkDerivation rec {
      pname = "caddy-using-xcaddy-${xcaddy.version}";
      inherit version;

      dontUnpack = true;
      dontFixup = true;

      nativeBuildInputs = [ cacert go ];

      plugins = [
        "github.com/mholt/caddy-l4@352413454ba91946a196ba37ee4e3f86f3cd733e"
        "github.com/mholt/caddy-events-exec@055bfd2e8b8247533c7a710e11301b7d1645c933"
      ];

      configurePhase = ''
        export GOCACHE="$TMPDIR/go-cache"
        export GOPATH="$TMPDIR/go"
        export XCADDY_SKIP_BUILD=1
      '';

      buildPhase = ''
        ${xcaddy}/bin/xcaddy build "${rev}" ${lib.concatMapStringsSep " " (plugin: "--with ${plugin}") plugins}
        cd buildenv*
        go mod vendor
      '';

      installPhase = ''
        cp -r --reflink=auto . $out
      '';

      outputHash = "sha256-kFxchD3wl1fbbT6P6BtlC+c4gnXI09UvXDZ3cl6lhu8=";
      outputHashMode = "recursive";
    };

    subPackages = [ "." ];
    ldflags = [ "-s" "-w" ];
    vendorHash = null;
  });
}
