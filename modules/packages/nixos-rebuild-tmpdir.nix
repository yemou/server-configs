# nixos-rebuild is a shellscript and inside that shellscript they create a tmpdir using mktemp.
# This is fine, but TMPDIR environment variable isn't available at this point, so mktemp puts the
# directory in the wrong place.
{ makeWrapper, nixos-rebuild, symlinkJoin }: symlinkJoin {
  name = "nixos-rebuild-tmpdir";
  paths = [ nixos-rebuild ];
  buildInputs = [ makeWrapper ];
  postBuild = ''
    wrapProgram $out/bin/nixos-rebuild \
      --set TMPDIR /nix/tmp
  '';
}
