{ age
, callPackage
, coreutils
, git
, nettools
, sops
, writeShellApplication
}: writeShellApplication {
  name = "buildConfig";
  runtimeInputs = [
    age
    coreutils
    git
    nettools
    sops
    (callPackage ../nixos-rebuild-tmpdir.nix { })
  ];
  text = builtins.readFile ./buildConfig.sh;
}
