#!/bin/sh
# Script to build nixos configuration with hidden values
# (these values will appear in the nix store)

# Steps:
#   1. Check hostname
#   2. Check if a hidden.json exists
#     a. If it does continue
#     b. if it doesn't then run the normal nixos-rebuild command
#        `sudo nixos-rebuild switch --flake /config#$HOSTNAME`
#   3. Copy all files into a temp directory that is only readable by root (use `git ls-files`)
#   4. Use `sops decrypt --in-place $HOSTNAME/hidden.json` to decrypt hidden values for use by nix
#   5. Run `sudo nixos-rebuild switch --flake $TEMPDIR#$HOSTNAME`
#   6. Remove temporary directory if build was sucessful

hostname=$(hostname)

[ -e "/config/$hostname/hidden.json" ] || {
	nixos-rebuild switch --flake "/config#$hostname" "$@"
	exit $?
}

temp_dir=$(mktemp -dt nix-config.XXX)

(
	cd /config || exit 1
	for file in $(git ls-files)
	do cp --parents "$file" "$temp_dir/"
	done
)

[ -e "$temp_dir/$hostname/hidden.json" ] || {
	printf '%s\n' "$hostname/hidden.json is not yet staged"
	exit 1
}

SOPS_AGE_KEY_FILE=${SOPS_AGE_KEY_FILE:=/data/keys.txt} \
	sops decrypt --in-place "$temp_dir/$hostname/hidden.json"

nixos-rebuild switch --flake "$temp_dir#$hostname" "$@"
rm -r "$temp_dir"
