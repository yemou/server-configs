{ pkgs, ... }: {
  environment.systemPackages = [ pkgs.rclone ];

  fileSystems."/sss" = {
    device = "sss:/data/sss";
    fsType = "rclone";
    depends = [ "/home/mou" ];
    options = [
      "allow_non_empty"
      "allow_other"
      "args2env"
      "config=${pkgs.writeText "rclone-mnt.conf" ''
        [sss]
        type = sftp
        host = 100.77.30.206
        user = seedling
        key_file = /data/seedling/id_ed25519
      ''}"
      "dir_cache_time=1m"
      "gid=1001"
      "nodev"
      "nofail"
      "poll_interval=30s"
      "sftp_md5sum_command=${pkgs.coreutils}/bin/md5sum"
      "sftp_sha1sum_command=${pkgs.coreutils}/bin/sha1sum"
      "uid=1000"
      "vfs_cache_mode=full"
    ];
  };
}
