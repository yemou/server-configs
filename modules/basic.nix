{ config, pkgs, yemou-scripts, ... }: {
  nixpkgs.overlays = [ yemou-scripts.overlays.default ];

  sops.secrets = {
    "passwordHashes/root".neededForUsers = true;
    "passwordHashes/mou".neededForUsers = true;
  };

  i18n.defaultLocale = "C.UTF-8";
  time.timeZone = "America/New_York";

  environment = {
    loginShellInit = ''
      if [ -e /etc/profiles/per-user/$USER/etc/profile.d/hm-session-vars.sh ]
      then . /etc/profiles/per-user/$USER/etc/profile.d/hm-session-vars.sh
      fi
    '';
    persistence."/data/persistent" = {
      hideMounts = true;
      directories = [
        "/var/log"
        "/var/lib/nixos"
        "/var/lib/systemd/coredump"
        { directory = "/var/lib/private"; mode = "0700"; }
      ];
    };
    sessionVariables = {
      XDG_CACHE_HOME = "$HOME/.cache";
      XDG_CONFIG_HOME = "$HOME/.config";
      XDG_DATA_HOME = "$HOME/.local/share";
      XDG_STATE_HOME = "$HOME/.local/state";
    };
    systemPackages = with pkgs; [
      git
      htop
      lsof
      man-pages
      man-pages-posix
      age # sops-nix
      sops # sops-nix
      magic-wormhole-rs
      thm # make colorscheme work properly
    ];
  };

  security.polkit.enable = true;
  services.acpid.enable = true;

  users = {
    groups = {
      mou.gid = 1000;
      seedling.gid = 1001;
    };
    users = {
      root.hashedPasswordFile = config.sops.secrets."passwordHashes/root".path;
      mou = {
        isNormalUser = true;
        uid = 1000;
        group = "mou";
        extraGroups = [ "users" "wheel" ];
        shell = pkgs.loksh;
        hashedPasswordFile = config.sops.secrets."passwordHashes/mou".path;
      };
      seedling = {
        isNormalUser = true;
        uid = 1001;
        group = "seedling";
        extraGroups = [ "users" ];
      };
    };
  };
}
